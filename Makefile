TEX = pdflatex -shell-escape -interaction nonstopmode -halt-on-error -file-line-error
CHK = chktex -shell-escape -interaction nonstopmode -halt-on-error -file-line-error
BIB = bibtex
MAIN = RadoslawRowicki_CV
SRC = $(MAIN).tex
OUT = $(MAIN).pdf
DICT = $(TEXTDIR)/.aspell.en.pws
REPL = $(TEXTDIR)/.aspell.en.prepl

.PHONY : all clean splchk

all : $(OUT) # check splchk

check:
	! $(CHK) $(MAIN) | grep .

clean :
	-rm -rf {*.{aux,log,pdf,toc,out,bbl,blg},auto}

$(OUT) :
	$(TEX) $(SRC)

%.chk: %.tex
	aspell \
		--home-dir=./$(TEXTDIR) \
		--personal=$(DICT) \
		--repl=$(REPL) \
		--lang=en_GB \
		--mode=tex \
		--add-tex-command="autoref op" \
		-x \
		check $<

splchk: $(DICT) $(REPL) $(addsuffix .chk,$(basename $(TEXT)))
